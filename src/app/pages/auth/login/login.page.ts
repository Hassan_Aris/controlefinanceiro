import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private serviceLogin: LoginService,
    private nav : NavController,
  ) { }

  ngOnInit() {   
    this.isUserLogged();
    this.loginForm = this.builder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }
  
  login() {
    const user = this.loginForm.value;
    this.serviceLogin.login(user);
  }

  isUserLogged(){
    this.serviceLogin.isLogged.subscribe(user => 
      {
        if(user){
          this.nav.navigateForward('home')
        }
      }
    );
  }
}
