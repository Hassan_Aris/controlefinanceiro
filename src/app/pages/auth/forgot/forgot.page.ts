import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {
  loginForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private serviceLogin: LoginService
  ) { }

  ngOnInit() {
    this.loginForm = this.builder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }
  RecoverPass() {
    const email = this.loginForm.value;
    this.serviceLogin.RecoverPass(email);
  }
}
