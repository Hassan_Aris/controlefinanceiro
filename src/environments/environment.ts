// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:  {
    apiKey: 'AIzaSyChvOTEH3Q-aMWgifq5VND4TyMiCAodk2k',
    authDomain: 'controle-financeiro-if.firebaseapp.com',
    projectId: 'controle-financeiro-if',
    storageBucket: 'controle-financeiro-if.appspot.com',
    messagingSenderId: '767066272154',
    appId: '1:767066272154:web:12e26ea38c8f0eb9d4ca23',
    measurementId: 'G-2513LXG9WQ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
