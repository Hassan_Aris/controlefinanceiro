import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLogged: Observable<User>;

  constructor(
    private nav: NavController, 
    private auth: AngularFireAuth,
    private toast: ToastController
  ) {
    this.isLogged = auth.authState;
  }

  login(user){
    this.auth.signInWithEmailAndPassword(user.email, user.password).
    then(() => this.nav.navigateForward('home')).catch(() => this.ErrorMenssager());
  }

  RecoverPass(email){
    this.auth.sendPasswordResetEmail(email.email).
    then(() => this.nav.navigateBack('login')).catch(() => this.ErrorMenssager());
  }

  CreateUser(user){
    this.auth.createUserWithEmailAndPassword(user.email, user.password);
  }

  logout(){
    this.auth.signOut().then(() => this.nav.navigateBack('login'));
  }
  private async ErrorMenssager(){
    const toastVariable = await this.toast.create({
      message: 'Dados Incorretos',
      duration: 3000
    });
    toastVariable.present();
  }

}
